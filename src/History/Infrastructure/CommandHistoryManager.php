<?php

namespace Jakmall\Recruitment\Calculator\History\Infrastructure;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryLog;

class CommandHistoryManager implements CommandHistoryManagerInterface {

    protected $filePath = "src/mesinhitung.log";
    protected $latestPath = "src/latest.log";
    protected $commandHistoryLog;

    public function __construct(){
        $command = new CommandHistoryLog();
        $this->commandHistoryLog = $command;
    }
    
    public function findAll($driver): array {
        return $this->commandHistoryLog->findAll($driver);
    }

    public function find($driver, $id) {

        if($driver == "file"){
            // Find data logs by file
            $data = $this->commandHistoryLog->findByIDCommand($this->filePath, $id);
        }elseif($driver == "latest"){
            // Find data logs by latest
            $data = $this->commandHistoryLog->findByIDCommand($this->latestPath, $id);
        }else{
            // Find data logs by latest
            $data = $this->commandHistoryLog->findByIDCommand($this->latestPath, $id);
            if(count($data) < 1){
                // Find data logs by file
                $data = $this->commandHistoryLog->findByIDCommand($this->filePath, $id);
            }
        }

        return $data;
    }

    public function log($command): bool {

        /**
         * Store data to mesinhitung.log
         */

        $idFile = $this->commandHistoryLog->logMesinHitung($command);

        /**
         * Store data to latest.log
         */

        $this->commandHistoryLog->logLatest($command, $idFile);
        

        return true;
    }

    public function clear($id): bool {
        $statusClear = $this->commandHistoryLog->clearByID($id);
        return $statusClear;
    }

    public function clearAll(): bool {
        $statusClear = $this->commandHistoryLog->clearAll();
        return $statusClear;
    }
}