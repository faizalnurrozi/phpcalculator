<?php

namespace Jakmall\Recruitment\Calculator\History\Infrastructure;

interface CommandHistoryLogInterface {

    public function logMesinHitung($command) : string;

    public function logLatest($command, $idFile) : void;
    
    public function findAll($driver) : array;
    
    public function findByID($sourceLog, $id) : array;
    
    public function findByIDCommand($sourceLog, $id) : array;

    public function clearAll() : bool;
    
    public function clearByID($id) : bool;

}