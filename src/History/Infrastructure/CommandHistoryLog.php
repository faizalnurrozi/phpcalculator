<?php

namespace Jakmall\Recruitment\Calculator\History\Infrastructure;

class CommandHistoryLog implements CommandHistoryLogInterface {

    protected $filePath = "src/mesinhitung.log";
    protected $latestPath = "src/latest.log";

    public function logMesinHitung($command) : string {
        $contentLogFile = file($this->filePath);
        $idFile = count($contentLogFile) + 1;

        $dataLogFile = $this->composeLog($idFile, $command["verb"], $command["operation"], $command["result"]);
        $dataLogFileImplode = implode("|", $dataLogFile);
        file_put_contents($this->filePath, $dataLogFileImplode . "\n", FILE_APPEND);

        return $idFile;
    }

    public function logLatest($command, $idFile) : void {
        $contentLogLatest = file($this->latestPath);
        $contentLogLatestOnly = file_get_contents($this->latestPath);

        // New command is containing
        if(strpos($contentLogLatestOnly, $command["operation"]) !== false){
            file_put_contents($this->latestPath, '');
            foreach($contentLogLatest as $log){
                $dataLog = explode("|", $log);
                if($dataLog[2] != $command["operation"]){
                    $dataLogLatest = $this->composeLog($dataLog[0], $dataLog[1], $dataLog[2], str_replace("\n", "", $dataLog[3]));
                    file_put_contents($this->latestPath, implode("|", $dataLogLatest) . "\n", FILE_APPEND);
                }
            }
        }

        $contentLogLatest = file($this->latestPath);
        $countLogLatest = count($contentLogLatest);
        if($countLogLatest > 9){
            file_put_contents($this->latestPath, '');

            $dataLogLatestNew = $this->composeLog($idFile, $command["verb"], $command["operation"], $command["result"]);

            $i = 0;
            foreach($contentLogLatest as $log){
                $dataLog = explode("|", $log);
                if($i > 0){
                    $dataLogLatest = $this->composeLog($dataLog[0], $dataLog[1], $dataLog[2], str_replace("\n", "", $dataLog[3]));

                    file_put_contents($this->latestPath, implode("|", $dataLogLatest) . "\n", FILE_APPEND);
                }
                $i++;
            }

            $dataLogLatestImplodeNew = implode("|", $dataLogLatestNew);
            file_put_contents($this->latestPath, $dataLogLatestImplodeNew . "\n", FILE_APPEND);
        }else{
            $dataLogLatest = $this->composeLog($idFile, $command["verb"], $command["operation"], $command["result"]);
            $dataLogLatestImplode = implode("|", $dataLogLatest);
            file_put_contents($this->latestPath, $dataLogLatestImplode . "\n", FILE_APPEND);
        }
    }

    public function findAll($driver) : array {
        $data = array();
        $sourceLog = $this->filePath;
        if($driver == "latest") $sourceLog = $this->latestPath;
        $sourceContent = file($sourceLog);
        for($i = count($sourceContent) - 1; $i >= 0; $i--){
            $contentSplit = explode("|", $sourceContent[$i]);
            $data[] = array(
                "id"        => $contentSplit[0],
                "verb"      => $contentSplit[1],
                "operation" => $contentSplit[2],
                "result"    => str_replace("\n", "", $contentSplit[3]),
            );
        }

        return $data;
    }

    public function findByID($path, $id) : array {
        $data = array();
        foreach(file($path) as $content){
            $contentSplit = explode("|", $content);
            if($contentSplit[0] == $id){
                $data[] = array(
                    "id"        => $contentSplit[0],
                    "verb"      => $contentSplit[1],
                    "operation" => $contentSplit[2],
                    "result"    => str_replace("\n", "", $contentSplit[3]),
                );
                break;
            }
        }

        return $data;
    }

    public function findByIDCommand($path, $id) : array {
        $data = $this->findByID($path, $id);
        
        // Update when read data by ID
        if($data[0]["id"] != ""){
            $this->logLatest($data[0], $data[0]["id"]);
        }

        return $data;
    }

    public function clearAll() : bool{

        /**
         * Clear all data in file mesinhitung.log
         */

        file_put_contents($this->filePath, "");

        /**
         * Clear all data in file latest.log
         */

        file_put_contents($this->latestPath, "");

        return true;
    }

    public function clearByID($id) : bool {

        $statusExist = false;

        /**
         * Get data by ID in file mesinhitung.log
         */

        $dataFileLog = $this->findByID($this->filePath, $id)[0];

        /**
         * Check if data is exist
         */

        if($dataFileLog["id"] != ""){

            $statusExist = true;
            $dataLogComposed = implode("|", $dataFileLog) . "\n";

            /**
             * Clear data by ID in file mesinhitung.log
             */

            $contentFileLog = file_get_contents($this->filePath);
            $contentFileLog = str_replace($dataLogComposed, "", $contentFileLog);
            file_put_contents($this->filePath, $contentFileLog);

            /**
             * Clear data by ID in file latest.log
             */

            $contentLatestLog = file_get_contents($this->latestPath);
            $contentLatestLog = str_replace($dataLogComposed, "", $contentLatestLog);
            file_put_contents($this->latestPath, $contentLatestLog);
        }

        return $statusExist;

    }

    private function composeLog($id, $command, $operation, $result) : array {
        $data = array(
            "id"        => $id,
            "command"   => $command,
            "operation" => $operation,
            "result"    => $result,
        );

        return $data;
    }

}