<?php

namespace Jakmall\Recruitment\Calculator\History\Factory;

abstract class History {
    
    abstract public function factoryMethod(): HistoryInterface;
    
}