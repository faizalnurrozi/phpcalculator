<?php

namespace Jakmall\Recruitment\Calculator\History\Factory;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManager;

class Composite implements HistoryInterface {

    public function findAll() : array {
        $commandHistoryManager = new CommandHistoryManager();
        return $commandHistoryManager->findAll("composite");
    }

    public function find($id) : array {
        $commandHistoryManager = new CommandHistoryManager();
        return $commandHistoryManager->find("composite", $id);
    }

}