<?php

namespace Jakmall\Recruitment\Calculator\History\Factory;

class HistoryComposite extends History {

    public function factoryMethod() : HistoryInterface{

        return new Composite();

    }

}