<?php

namespace Jakmall\Recruitment\Calculator\History\Factory;

interface HistoryInterface {
    
    public function findAll() : array;
    
    public function find($id) : array;

}