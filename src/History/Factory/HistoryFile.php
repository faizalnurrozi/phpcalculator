<?php

namespace Jakmall\Recruitment\Calculator\History\Factory;

class HistoryFile extends History {

    public function factoryMethod() : HistoryInterface{

        return new File();

    }

}