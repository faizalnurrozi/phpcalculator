<?php

namespace Jakmall\Recruitment\Calculator\History\Factory;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManager;

class Latest implements HistoryInterface {

    public function findAll() : array {
        $commandHistoryManager = new CommandHistoryManager();
        return $commandHistoryManager->findAll("latest");
    }

    public function find($id) : array {
        $commandHistoryManager = new CommandHistoryManager();
        return $commandHistoryManager->find("latest", $id);
    }

}