<?php

namespace Jakmall\Recruitment\Calculator\History\Factory;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManager;

class File implements HistoryInterface {

    public function findAll() : array {
        $commandHistoryManager = new CommandHistoryManager();
        return $commandHistoryManager->findAll("file");
    }

    public function find($id) : array {
        $commandHistoryManager = new CommandHistoryManager();
        return $commandHistoryManager->find("file", $id);
    }

}