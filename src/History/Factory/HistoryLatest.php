<?php

namespace Jakmall\Recruitment\Calculator\History\Factory;

class HistoryLatest extends History {

    public function factoryMethod() : HistoryInterface{

        return new Latest();

    }

}