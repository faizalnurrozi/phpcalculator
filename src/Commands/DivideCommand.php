<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\Commands\Operation\OperatorDivide;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class DivideCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;
    protected $logManager;
    protected $factory;

    public function __construct(CommandHistoryManagerInterface $logManager)
    {
        /**
         * Initiate facoty
         */

        $factory = new OperatorDivide();
        $this->factory = $factory;

        /**
         * Initiate logManager
         */

        $this->logManager = $logManager;

        /**
         * Get command verb
         */

        $commandVerb = $this->getCommandVerb();

        /**
         * Setup signature
         */

        $this->signature = sprintf(
            '%s {numbers* : The numbers to be %s}',
            $commandVerb,
            $this->getCommandPassiveVerb()
        );
        $this->description = sprintf('%s all given Numbers', ucfirst($this->getCommandVerb()));
        parent::__construct();
    }

    protected function getCommandVerb(): string
    {
        return 'divide';
    }

    protected function getCommandPassiveVerb(): string
    {
        return 'divided';
    }

    public function handle(): void
    {
        $numbers = $this->getInput();
        $description = $this->generateCalculationDescription($numbers);
        $result = $this->calculateAll($numbers);

        $this->comment(sprintf('%s = %s', $description, $result));

        /**
         * Append log
         */
        $dataComamndLog = array(
            "verb" => $this->getCommandVerb(),
            "operation" => $description,
            "result" => $result,
        );
        $this->logManager->log($dataComamndLog);
    }

    protected function getInput(): array
    {
        return $this->argument('numbers');
    }

    protected function generateCalculationDescription(array $numbers): string
    {
        $operator = $this->getOperator();
        $description = $this->factory->factoryMethod()->generateCalculationDescription($numbers, $operator);

        return $description;
    }

    protected function getOperator(): string
    {
        return '/';
    }

    /**
     * @param array $numbers
     *
     * @return float|int
     */
    protected function calculateAll(array $numbers)
    {
        $result = $this->factory->factoryMethod()->calculateAll($numbers);
        return $result;
    }
}
