<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\Commands\Operation\OperatorPower;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class PowerCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;
    protected $logManager;
    protected $factory;

    public function __construct(CommandHistoryManagerInterface $logManager)
    {
        /**
         * Initiate facoty
         */

        $factory = new OperatorPower();
        $this->factory = $factory;

        /**
         * Initiate logManager
         */

        $this->logManager = $logManager;

        /**
         * Get command verb
         */

        $commandVerb = $this->getCommandVerb();

        /**
         * Setup signature
         */

        $this->signature = sprintf(
            '%s {base : The base numbers} {exp : The exponent number}',
            $commandVerb
        );
        $this->description = sprintf('Exponent the given number');
        parent::__construct();
    }

    protected function getCommandVerb(): string
    {
        return 'power';
    }
    
    public function handle(): void
    {
        $base = $this->getBaseInput();
        $exp = $this->getExpInput();
        $description = $this->generateCalculationDescription($base, $exp);

        $numbers = array($base, $exp);
        $result = $this->calculateAll($numbers);

        $this->comment(sprintf('%s = %s', $description, $result));

        /**
         * Append log
         */
        $dataComamndLog = array(
            "verb" => $this->getCommandVerb(),
            "operation" => $description,
            "result" => $result,
        );
        $this->logManager->log($dataComamndLog);
    }

    protected function getBaseInput()
    {
        return $this->argument('base');
    }

    protected function getExpInput()
    {
        return $this->argument('exp');
    }

    protected function generateCalculationDescription(int $base, int $exp): string
    {
        $operator = $this->getOperator();
        $numbers = array($base, $exp);
        $description = $this->factory->factoryMethod()->generateCalculationDescription($numbers, $operator);
        return $description;
    }

    protected function getOperator(): string
    {
        return '^';
    }

    /**
     * @param int|float $number1
     * @param int|float $number2
     *
     * @return int|float
     */
    protected function calculateAll(array $numbers)
    {
        $result = $this->factory->factoryMethod()->calculateAll($numbers);
        return $result;
    }
}
