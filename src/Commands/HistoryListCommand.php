<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;
use Jakmall\Recruitment\Calculator\History\Factory\History;
use Jakmall\Recruitment\Calculator\History\Factory\HistoryComposite;
use Jakmall\Recruitment\Calculator\History\Factory\HistoryFile;
use Jakmall\Recruitment\Calculator\History\Factory\HistoryLatest;

class HistoryListCommand extends Command {
    
    protected $signature;
    protected $description;
    protected $logManager;

    public function __construct(CommandHistoryManagerInterface $logManager){
        $this->logManager = $logManager ;

        $commandVerb = $this->getCommandVerb();

        $this->signature = sprintf(
            '%s {id?* : Filter the history by ID} {--D|driver=composite : Driver for source file [file|latest|composite]}',
            $commandVerb
        );
        $this->description = "Show calculator history";

        parent::__construct();
    }

    protected function getCommandVerb(): string{
        return 'history:list';
    }

    public function handle(): void{
        $commandsID = $this->argument('id');
        $driver = $this->option('driver');

        switch($driver){
            case "file":
                $sourceLog = new HistoryFile();
                break;

            case "latest":
                $sourceLog = new HistoryLatest();
                break;

            case "composite":
            default:
                $sourceLog = new HistoryComposite();
                break;
        }

        $headers = ['ID', 'Command', 'Operation', 'Result'];
        $hisotyLog  = $this->history($sourceLog);

        if(@$commandsID[0] == ""){
            $dataLogs = $hisotyLog->findAll();
        }else{
            $id = $commandsID[0];
            $dataLogs = $hisotyLog->find($id);
        }

        if(count($dataLogs) > 0)
            $this->table($headers, $dataLogs);
        else
            $this->info("History is empty.");

    }

    private function history(History $history) {
        return $history->factoryMethod();
    }

}