<?php

namespace Jakmall\Recruitment\Calculator\Commands\Operation;

class OperatorPower extends Operator {

    public function factoryMethod() : OperatorInterface{

        return new Power();

    }

}