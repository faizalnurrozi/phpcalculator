<?php

namespace Jakmall\Recruitment\Calculator\Commands\Operation;

class OperatorSubtract extends Operator {

    public function factoryMethod() : OperatorInterface{

        return new Subtract();

    }

}