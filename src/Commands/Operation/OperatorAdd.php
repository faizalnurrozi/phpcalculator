<?php

namespace Jakmall\Recruitment\Calculator\Commands\Operation;

class OperatorAdd extends Operator {

    public function factoryMethod() : OperatorInterface{

        return new Add();

    }

}