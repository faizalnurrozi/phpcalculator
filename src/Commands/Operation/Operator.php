<?php

namespace Jakmall\Recruitment\Calculator\Commands\Operation;

abstract class Operator {

    abstract public function factoryMethod(): OperatorInterface;

}