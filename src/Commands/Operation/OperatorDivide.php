<?php

namespace Jakmall\Recruitment\Calculator\Commands\Operation;

class OperatorDivide extends Operator {

    public function factoryMethod() : OperatorInterface{

        return new Divide();

    }

}