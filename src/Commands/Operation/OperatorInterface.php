<?php

namespace Jakmall\Recruitment\Calculator\Commands\Operation;

interface OperatorInterface {
    
    public function generateCalculationDescription(array $numbers, $operator) : string;
    
    public function calculateAll(array $numbers);

}