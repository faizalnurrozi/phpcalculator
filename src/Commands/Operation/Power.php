<?php

namespace Jakmall\Recruitment\Calculator\Commands\Operation;

class Power implements OperatorInterface {

    public function generateCalculationDescription(array $numbers, $operator) : string {
        $base = $numbers[0];
        $exp = $numbers[1];
        return sprintf('%s %s %s', $base, $operator, $exp);
    }
    
    public function calculateAll(array $numbers) {
        $base = $numbers[0];
        $exp = $numbers[1];

        return $this->calculate($base, $exp);
    }

    protected function calculate($base, $exp) {
        return pow($base, $exp);
    }

}