<?php

namespace Jakmall\Recruitment\Calculator\Commands\Operation;

class Add implements OperatorInterface {

    public function generateCalculationDescription(array $numbers, $operator) : string {
        $glue = sprintf(' %s ', $operator);
        return implode($glue, $numbers);
    }
    
    public function calculateAll(array $numbers) {
        $number = array_pop($numbers);

        if (count($numbers) <= 0) {
            return $number;
        }

        return $this->calculate($this->calculateAll($numbers), $number);
    }

    protected function calculate($number1, $number2) {
        return $number1 + $number2;
    }

}