<?php

namespace Jakmall\Recruitment\Calculator\Commands\Operation;

class OperatorMultiply extends Operator {

    public function factoryMethod() : OperatorInterface{

        return new Multiply();

    }

}