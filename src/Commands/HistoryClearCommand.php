<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class HistoryClearCommand extends Command {

    protected $signature;
    protected $description;
    protected $logManager;

    public function __construct(CommandHistoryManagerInterface $logManager){
        $this->logManager = $logManager ;

        $commandVerb = $this->getCommandVerb();

        $this->signature = sprintf(
            '%s {id?* : Delete the history by ID or All [empty <id> for clear all history]}',
            $commandVerb
        );
        $this->description = "Clear calculator history";

        parent::__construct();
    }

    protected function getCommandVerb(): string{
        return 'history:clear';
    }

    public function handle(): void {
        $commandsID = $this->argument('id');

        if(@$commandsID[0] == ""){

            /**
             * Clear all data
             */

            $this->logManager->clearAll();

            /**
             * Print status cleared
             */
            
            $this->info("History has been cleared");
        }else{

            /**
             * Clear data by ID
             */

            $id = $commandsID[0];
            $statusClear = $this->logManager->clear($id);

            /**
             * Print status cleared
             */

            if(!$statusClear)
                $this->warn("History is not found");
            else
                $this->info("History has been deleted");
        }
    }
    
}