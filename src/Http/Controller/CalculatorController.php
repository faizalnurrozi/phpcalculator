<?php

namespace Jakmall\Recruitment\Calculator\Http\Controller;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Jakmall\Recruitment\Calculator\Commands\Operation\OperatorAdd;
use Jakmall\Recruitment\Calculator\Commands\Operation\OperatorDivide;
use Jakmall\Recruitment\Calculator\Commands\Operation\OperatorMultiply;
use Jakmall\Recruitment\Calculator\Commands\Operation\OperatorPower;
use Jakmall\Recruitment\Calculator\Commands\Operation\OperatorSubtract;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManagerInterface;

class CalculatorController
{
    protected $logManager;
    protected $action;
    protected $operator;

    public function __construct(CommandHistoryManagerInterface $logManager){
        $this->logManager = $logManager;
        $this->action   = ["add", "subtract", "multiply", "divide", "power"];
        $this->operator = array(
            $this->action[0] => "+",
            $this->action[1] => "-",
            $this->action[2] => "*",
            $this->action[3] => "/",
            $this->action[4] => "^",
        );
    }

    public function calculate(Request $request, $action){

        $action = strtolower($action);

        /**
         * Check action is registered
         */

        if(!in_array($action, $this->action)) {
            return JsonResponse::create([
                "message" => "Action is invalid"
            ], 400);
        }

        /**
         * Check request input
         */
        
        $inputs = $request->input('input');
        if(!is_array($inputs)) {
            return JsonResponse::create([
                'message' => 'Input is invalid, input must be array input[]'
            ], 400);
        }

        /**
         * Init factory by action
         */

        switch($action){
            case "add":
                $factory = new OperatorAdd();
                break;

            case "subtract":
                $factory = new OperatorSubtract();
                break;

            case "multiply":
                $factory = new OperatorMultiply();
                break;

            case "divide":
                $factory = new OperatorDivide();
                break;

            case "power":
                $factory = new OperatorPower();
                break;
        }

        /**
         * Get description and result from the factory
         */

        $description = $factory->factoryMethod()->generateCalculationDescription($inputs, $this->operator[$action]);
        $result = $factory->factoryMethod()->calculateAll($inputs);

        /**
         * Store operation and result to log
         */

        $dataComamndLog = array(
            "verb"      => $action,
            "operation" => $description,
            "result"    => $result,
        );
        $this->logManager->log($dataComamndLog);

        /**
         * Response json beside data action, description and result
         */

        return JsonResponse::create([
            'command'   => $action,
            'operation' => $description,
            'result'    => $result
        ], 201);

    }
}
