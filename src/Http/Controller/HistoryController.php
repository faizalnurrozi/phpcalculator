<?php

namespace Jakmall\Recruitment\Calculator\Http\Controller;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Jakmall\Recruitment\Calculator\History\Factory\History;
use Jakmall\Recruitment\Calculator\History\Factory\HistoryComposite;
use Jakmall\Recruitment\Calculator\History\Factory\HistoryFile;
use Jakmall\Recruitment\Calculator\History\Factory\HistoryLatest;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryLog;
use Jakmall\Recruitment\Calculator\History\Infrastructure\CommandHistoryManager;

class HistoryController
{

    protected $action;
    protected $operator;
    protected $logManager;

    public function __construct()
    {
        $this->action   = ["add", "subtract", "multiply", "divide", "power"];
        $this->operator = array(
            $this->action[0] => "+",
            $this->action[1] => "-",
            $this->action[2] => "*",
            $this->action[3] => "/",
            $this->action[4] => "^",
        );

        /**
         * Initiate log manager
         */

        $logManager = new CommandHistoryManager();
        $this->logManager = $logManager;
    }

    public function index(Request $request)
    {

        /**
         * Get parameter driver
         */

        $driver = $request->input('driver');
        if($driver == "") $driver = "composite";

        /**
         * Initiate history factory
         */

        $sourceLog = $this->getDriver($driver);

        $dataLogs = $sourceLog->factoryMethod()->findAll();
        $logsComposed = $this->composeLogs($dataLogs);

        if(count($logsComposed) > 0){
            return JsonResponse::create($logsComposed, 201);
        }else{
            return JsonResponse::create([], Response::HTTP_NO_CONTENT);
        }
    }

    public function show(Request $request, $id)
    {

        /**
         * Get parameter driver
         */

        $driver = $request->input('driver');
        if($driver == "") $driver = "composite";

        /**
         * Initiate history factory
         */

        $sourceLog = $this->getDriver($driver);

        $dataLogs = $sourceLog->factoryMethod()->find($id);
        $logsComposed = $this->composeLogs($dataLogs);

        if(count($logsComposed) > 0){
            return JsonResponse::create($logsComposed, 201);
        }else{
            return JsonResponse::create([
                "message" => Response::$statusTexts[Response::HTTP_UNPROCESSABLE_ENTITY]
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

    }

    public function remove($id)
    {
        $this->logManager->clear($id);
        return JsonResponse::create([], Response::HTTP_NO_CONTENT);
    }

    private function composeLogs(array $logs) : array {
        $data = array();
        $indexData = 0;
        foreach($logs as $log) {
            $id         = $log["id"];
            $action     = $log["verb"];
            $operation  = $log["operation"];
            $result     = $log["result"];

            $operator   = $this->operator[$action];
            $input      = explode($operator, $operation);
            $inputs     = array();
            foreach($input as $dataInput){
                $dataInput = trim($dataInput);
                $inputs[] = (int) $dataInput;
            }

            $data[$indexData]["id"]         = $id;
            $data[$indexData]["command"]    = $action;
            $data[$indexData]["operation"]  = $operation;
            $data[$indexData]["input"]      = $inputs;
            $data[$indexData]["result"]     = $result;

            $indexData++;
        }

        return $data;
    }

    private function getDriver($driver) : History {
        switch($driver){
            case "file":
                $sourceLog = new HistoryFile();
                break;

            case "latest":
                $sourceLog = new HistoryLatest();
                break;

            case "composite":
            default:
                $sourceLog = new HistoryComposite();
                break;
        }

        return $sourceLog;
    }
}
